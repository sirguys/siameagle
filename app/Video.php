<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
      'video_name_th',
      'video_name_en',
      'video_name_cn',
      'url_th',
      'url_en',
      'url_cn',
      'video_idTH',
      'video_idEN',
      'video_idCN'
    ];
}
