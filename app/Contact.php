<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
      'company_name_th',
      'company_name_en',
      'company_address_th',
      'company_address_en',
      'company_tel',
      'company_fax',
      'company_email',
      'company_fooice_house',
      'company_google_map',
      'company_image_map'
    ];
}
