<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

class AdminController extends Controller
{
    public function getIndex() {
      return view('admin.index');
    }

    public function getLogout() {
      if (Auth::check()) {
        Auth::logout();
        return redirect('auth/login');
      }
      else {
        return back();
      }
    }
}
