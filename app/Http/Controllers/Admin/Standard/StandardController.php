<?php

namespace App\Http\Controllers\Admin\Standard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StandardController extends Controller
{
    public function getIndex() {
      return view('admin.standard.index');
    }

    public function getCreate() {
      return view('admin.standard.create');
    }

    public function postCreate(Request $request) {
      dd($request->all());
    }

    public function getUpdate() {

    }

    public function postUpdate(Request $request) {

    }
}
