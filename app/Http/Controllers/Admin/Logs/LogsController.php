<?php

namespace App\Http\Controllers\Admin\Logs;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class LogsController extends Controller
{
   public function getIndex() {
      return view('admin.logs.index');
    }
}

class Article implements LogsActivityInterface {

   use LogsActivity;
       public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return 'Article "' . $this->name . '" was created';
        }

        if ($eventName == 'updated')
        {
            return 'Article "' . $this->name . '" was updated';
        }

        if ($eventName == 'deleted')
        {
            return 'Article "' . $this->name . '" was deleted';
        }

        return '';
    }
}
