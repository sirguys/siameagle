<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
      'news_article_th',
      'news_detail_th',
      'news_article_en',
      'news_detail_en',
      'news_article_cn',
      'news_detail_cn',
      'news_image_th',
      'news_image_en',
      'news_image_cn'
    ];
}
