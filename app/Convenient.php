<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenient extends Model
{
    protected $fillable = [
      'convenient_name_th',
      'convenient_detail_th',
      'convenient_name_en',
      'convenient_detail_en',
      'convenient_name_cn',
      'convenient_detail_cn',
      'convenient_image',
      'convenient_image_en',
      'convenient_image_cn'
    ];
}
