<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="Keywords" content="อินทรีสยาม,อินทรีแดง,เครื่องดื่มชูกำลัง,ดื่มได้ไม่จำกัด,EagleSiam">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <title>Eagle Siam - @yield('title')</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('assets/js/slippry.min.js')}}"></script> 
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    
    <link rel="stylesheet" href="{{asset('assets/css/slippry.css')}}" />
    @yield('style')
  </head>
  <body>
    <header class="main_header" >

        <div class="row">
            <div class="container" style="padding:0 2em;">
                <a class="logo" href="{{ url('home') }}"><img src="img/c0_img_logoo.png" alt="" />
                </a>
                <div class="mobile-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav>
              <p class="nav--img__language" style="margin:0;text-align:right;">
                  <img src="img/c0_ico_language_th_1.png" alt="" />
                  <strong style="color:#4C4C4C;">|</strong>
                  <img src="img/c0_ico_language_en_0.png" alt="" />
                  <strong style="color:#4C4C4C;">|</strong>
                  <img src="img/c0_ico_language_ch_0.png" alt="" />
                  <!-- <img src="img/c0_ico_social_facebook.png" alt="" />
                  <img src="img/c0_ico_social_twitter.png" alt="" />
                  <img src="img/c0_ico_social_youtube.png" alt="" /> -->
              </p>
                    <ul>
                        <li><a class="{{ Request::segment(1) == 'home' ? 'active' : '' }}" href="{{ url('home') }}">หน้าแรก</a>
                        </li>
                        <li><a class="{{ Request::segment(1) == 'all_news' ? 'active' : '' }}" href="{{ url('all_news') }}">ข่าวและกิจกรรม</a>
                        </li>
                        <li><a class="{{ Request::segment(1) == 'inseefai' ? 'active' : '' }}" href="{{ url('inseefai') }}">เครื่องดื่มอินทรีสยาม</a>
                        </li>
                        <li><a class="{{ Request::segment(1) == 'video_list' ? 'active' : '' }}" href="{{ url('video_list') }}">วิดีโอโฆษณา</a>
                        </li>
                        <li><a class="{{ Request::segment(1) == 'about' ? 'active' : '' }}" href="{{ url('about') }}">มาตรฐานการผลิต</a>
                        </li>
                        <li><a class="{{ Request::segment(1) == 'contact' ? 'active' : '' }}" href="{{ url('contact') }}">ติดต่อเรา</a>
                        </li>
                    </ul>
                </nav>
            </div>

    <!-- For 800px -->

            <div class="content">
              <div class="logo--800">
                <a href="index.php"> <img style="background:red;" src="img/c0_ico_language_th_1.png" alt="" /></a>
                <a href="index.php"> <img src="img/c0_ico_language_en_0.png" alt="" /></a>
                <a href="index.php"> <img src="img/c0_ico_language_ch_0.png" alt="" /></a>
                      <a href="index.php"><img src="img/c0_img_logo.png" alt="" /></a>
                 <a href="index.php"> <img src="img/c0_ico_social_facebook.png" alt="" /></a>
                 <a href="index.php"> <img src="img/c0_ico_social_twitter.png" alt="" /></a>
                 <a href="index.php"> <img src="img/c0_ico_social_youtube.png" alt="" /></a>
                 <ul>
                     <li class="col-xs-4" style=""><a href="inseefai.php">หน้าแรก</a>
                     </li>
                     <li class="col-xs-4" style=""><a href="all_news.php">ข่าวและกิจกรรม</a>
                     </li>
                     <li class="col-xs-4" style=""><a href="product.php">เครื่องดื่มอินทรีสยาม</a>
                     </li>

                     <li class="col-xs-4" style=""><a href="video_list.php">วิดีโอโฆษณา</a>
                     </li>
                     <li class="col-xs-4" style=""><a href="about.php">มาตรฐานการผลิต</a>
                     </li>
                     <li class="col-xs-4" style=""><a href="contact.php">ติดต่อเรา</a>
                     </li>
                 </ul>
              </div>
                <div class="mobile-toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav>
              <p class="nav--img__language--800" style="">
                  <!-- <img src="img/c0_ico_language_th_1.png" alt="" />
                  <img src="img/c0_ico_language_en_0.png" alt="" />
                  <img src="img/c0_ico_language_ch_0.png" alt="" />
                  <strong style="color:#e91c28;"></strong>
                  <img src="img/c0_ico_social_facebook.png" alt="" />
                  <img src="img/c0_ico_social_youtube.png" alt="" />
                  <img src="img/c0_ico_social_instagram.png" alt="" /> -->
              </p>
                    <!-- <ul>
                        <li><a href="inseefai.php">อินทรีไฟ</a>
                        </li>
                        <li><a href="news.php">ข่าวและกิจกรรม</a>
                        </li>
                        <li><a href="product.php">ประโยชน์</a>
                        </li>
                        <li><a href="who.php">แหมาะกับใคร</a>
                        </li>
                        <li><a href="about.php">มาตรฐานการผลิต</a>
                        </li>
                        <li><a href="contact.php">ติดต่อเรา</a>
                        </li>
                    </ul> -->
                </nav>
            </div>



        </div>

    </header>

    @yield('content')


    <div class="container footer">
      <footer>
        <a href="{{ url('home') }}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'home' ? 'active' : '' }} ">หน้าแรก</a>
        <a href="{{ url('all_news') }}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'all_news' ? 'active' : ''  }} ">ข่าวและกิจกรรม</a>
        <a href="{{ url('inseefai') }}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'inseefai' ? 'active' : '' }}">เครื่องดื่มอินทรีสยาม</a>
        <a href="{{ url('video_list') }}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'video_list' ? 'active' : '' }}">วิดีโอโฆษณา</a>
        <a href="{{ url('about') }}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'about' ? 'active' : '' }}">มาตรฐานการผลิต</a>
        <a href="{{ url('contact')}}" class="col-xs-6 col-sm-4 col-md-2 {{ Request::segment(1) == 'contact' ? 'active' : ''}}">ติดต่อเรา</a>
      </footer>
    </div>
    <div class="container-fluid footer-bottom">
      <div class="col-md-12" style="text-align:center;color:#fff;">
            <h3>บริษัท 1688 เวิลด์ไวด์ จำกัด</h3>
            <h6>13/47 หมู่ 7 ถ.นวมินทร์ แขวงคลองทุ่ม เขตบึ่งกุ่ม กรุงเทพฯ 10230</h6>
            <h6 style="color:#000;">@ 2015 1688 WORLDWIDE LTD.</h6>
      </div>

    </div>
    <!-- Scroll-to-Top -->
    <div>
        <a href="#" class="scroll2top"><img src="img/btn_back_to_top.png" style="margin-right:50px;">
        </a>
    </div>
      
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
    <script src="{{asset('assets/js/prefixfree.min.js')}}"></script>
    <script src="{{asset('assets/js/modernizr.js')}}" type="text/javascript"></script>
     
    <script src="assets/js/index.js"></script>


  </body>
</html>
