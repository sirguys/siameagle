@extends('layouts.main')
@section('title','Home')
@section('style')
<style>
/* News */
.news-slider {
/* Style the actual content */
}
.news-slider .text-content {
position: absolute;
top: 0;
left: 0;
right: 0;
/*background-color: rgba(255, 255, 255, 0.9);*/
background-color: #fff;
padding: 8em 0.8em;
width: 40%;
height: 100%;
-webkit-clip-path: polygon(0 0, 43% 0, 100% 100%, 0% 100%);
clip-path: polygon(0 0, 43% 0, 100% 100%, 0% 100%);
}
.news-slider .text-content h2.home--h2 {
/*margin: 0 0 0 0;*/
padding:0 0 0 1em;
background: rgba(233,28,40,0.7);
width: 60%;


}
.news-slider .text-content h2.home--h2:hover {
/*margin: 0 0 0 0;*/
/*padding:0 0 1em 1em;*/
background: #f5cb25;
}
.news-slider .text-content h2.home--h2 a:hover {
/*margin: 0 0 0 0;*/
/*padding:0 0 1em 1em;*/
color:rgb(233,28,40);
text-decoration: none;
}

.news-slider .text-content p {
margin: 1em 0;
line-height: 1.5;
}
.news-slider .text-content a.button-link {
padding: 0.25em 0.5em;
position: absolute;
bottom: 1em;
right: 1em;
}
.news-slider .image-content {
line-height: 0;
}
.news-slider .image-content img {
width: 100%;
height: 30em;
}
.news-slider .news-pager {
text-align: left;
display: block;
margin: 0.2em 3em 0;
padding: 0;
list-style: none;
}
.news-slider .news-pager li {
display: inline-block;
padding: 0.4em;
/*margin: 0 0 0 1em;*/
}
.news-slider .news-pager li.sy-active a {
color: #e91c28;
}
.news-slider .news-pager li a {
font-weight: 500;
font-size: 2em;
text-decoration: none;
display: block;
color: gray;
margin: -6px;
}
</style>
 <script type="text/javascript">
 $(document).ready(function(){

   jQuery('#news-demo').slippry({
  // general elements & wrapper
  slippryWrapper: '<div class="sy-box news-slider" />', // wrapper to wrap everything, including pager
  elements: 'article', // elments cointaining slide content

  // options
  adaptiveHeight: false, // height of the sliders adapts to current
  captions: false,

  // pager
  pagerClass: 'news-pager',

  // transitions
  transition: 'fade', // fade, horizontal, kenburns, false
  speed: 1200,
  pause: 8000,

  // slideshow
  autoDirection: 'next'
  });
});

 </script>
 @stop
 @section('content')

<div class="sections">
    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6 col-sm-offset-6">
                    <img class="img-responsive " src="img/f18.png" alt="" style="position:absolute;width:200px;float:right;right: 9em;"/>
                    <img class="img-responsive " src="img/f18.png" alt="" style="position:absolute;width:100px;float:right;bottom:-10em;right:-6em;"/>
                    <img class="img-responsive " src="img/f18.png" alt="" style="position:absolute;width:100px;float:right;bottom:-13em;right:2em;"/>
                        
                    <img class="img-responsive " src="img/balloon_festival_5.png" alt="" style="position:absolute;width:80px;float:right;right: 9em;top: 20em;"/>
                    <img class="img-responsive " src="img/balloon_festival_2.png" alt="" style="position:absolute;width:100px;float:right;bottom:-10em;right:-6em;top: 13em;"/>
                    <img class="img-responsive " src="img/balloon_festival_3.png" alt="" style="position:absolute;width:70px;float:right;bottom:-13em;right:-2em;top:17em;"/>    
                    </div>
                    <div class="col-sm-6">
                        
                        
                    <img class="img-responsive " src="img/balloon_festival_5.png" alt="" style="position:absolute;width:80px;float:left;right: 40em;top: 10em;"/>
                    <img class="img-responsive " src="img/balloon_festival_2.png" alt="" style="position:absolute;width:100px;float:left;bottom:-10em;"/>
                    <img class="img-responsive " src="img/balloon_festival_3.png" alt="" style="position:absolute;width:50px;float:left;bottom:-13em;right:20em;top:23em;"/>    
                    </div>
                    <div><img class="img-responsive can01_title" src="img/c1_img_can01_title.png" alt="" /> </div>
                    
                    <div class="row" style="top:200px;">
                    <div class="col-md-6"><span><center><img class="img-responsive " src="img/star_left.png" alt="" style="position:relative;top:-22em;left:5em;" /></center> </span></div>
                    <div class="col-md-6"><span><center><img class="img-responsive " src="img/star_right.png" alt="" style="position:relative;top:-22em;right:5em;" /></center> </span></div>
                    </div>
                    <div id="" class="animated "><img src="" alt="" />
                    </div>
                    <div class="non_animated bottle"><img src="" alt="" />
                    </div>

                </div>
            </div>
        </div>

        
     
        
        
        <span><img class="img-responsive can01_title--480" src="img/c1_img_can01_title_480.png" alt=""/> </span>
        <div class="container">
            <div class="row" style="bottom:340px;">
                <div class="col-xs-12 col-sm-6 div_right">
<!--                    <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ</strong>-->
                </div>
                <div class="col-xs-12 col-sm-6 div_left">
<!--                    <strong>  บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>-->
                </div>
            </div>

        </div>

        <div class="container">
            <div class="row" style="bottom:200px;">
                <div class="col-xs-12 div_480">
<!--
                    <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ<br>
                     บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>
-->
                </div>
            </div>

        </div>

    </section>

    <section class="row" id="sec01">
        <div class="container">
            <div class="row">
                <h1><img src="img/c2_ico_news.png" alt="" />&nbspข่าวสารและกิจกรรม</h1>

                <section id="news-demo">
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...</p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="img/album_other-20110506-175010-3.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...  </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="img/article-71342_1280.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล... </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="img/photographer-410326_1280.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล...</p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="img/album_other-20110506-175010-3.jpg" alt=""></div>
                  </article>
                  <article>
                    <div class="text-content">
                      <h2 class="home--h2" style="text-align:left;"><a href="news.php">ปลูกป่า ประสานใจอาสา</a></h2>
                      <div class="col-sm-12" style="width:60%;margin:0 20px; ;">
                        <p style="color: gray;">โครงการปลูกไม้เฉลิมพระเกียรติ สมเด็จ พระนางเจ้าสิริกิติ์ พระบรมราชินีนาถ เนื่องในโอกาสพระพิธีมหามงคล... </p>
                      </div>
                      <a href="#!" class="button-link read-more" style="color:gray;font-size:16px;right:5em;">ดูเพิ่มเติม</a>
                    </div>
                    <div class="image-content"><img src="img/photographer-410326_1280.jpg" alt=""></div>
                  </article>
                </section>

                <!-- <div id="slides" style="margin-bottom:50px;">
                <a href="news.php">  <img src="img/example-slide-1.jpg" alt=""></a>
                    <img src="img/example-slide-2.jpg" alt="">
                    <img src="img/example-slide-3.jpg" alt="">
                    <img src="img/example-slide-4.jpg" alt="">
                </div> -->

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <h1>คนทำงาน</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/images_14092812281.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">เคล็ดลับทำงานดีมีความสุข</a></h3>
                                    <p>ใครๆก็บอกว่า ถ้าได้ทำงานที่เรารัก เราจะมีความสุขกับการทำงาน แต่คนส่วนใหญ่ก็ยังมักบ่นว่าเบื่องานอยู่ดี...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/images_14092812281.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">เช็ค 5 เคล็ดลับ ขจัดขี้เกียจ</a></h3>
                                    <p>ทุกวันนี้พนักงานออฟฟิศล้วนเจอปัญหาเรื่องความขี้เกียจในตัวเองที่กำลังเป็นศัตรูตัวฉกาจของคนทำงานอย่างน่ากลัว...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>กีฬา</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/14502711731450271207l.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">'ริเวอร์เพลท' เฉือนชนะซานเฟรชเช่ ลิ่วชิงสโมสรโลก</a></h3>
                                    <p>การแข่งขันฟุตบอลชิงแชมป์สโมสรโลก ฟีฟ่า คลับ เวิลด์ คัพ ที่ประเทศญี่ปุ่นเมื่อวันที่ 16 ธ.ค. รอบรองชนะเลิศ...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/14502982591450298600l.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">แชมป์เมเจอร์ตบเท้าลง "เอสเอ็มบีซี สิงคโปร์ โอเฟน"</a></h3>
                                    <p>ดาร์เรน คลาร์ก และ หยาง ยอง-อุน (วาย อี หยาง) สองแชมป์เมเจอร์ ยืนยันลงดวลสะวิงศึก...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>เกษตรกรรม</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/B4572.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">สสส.6 คืนความสุข จัดเวทีส่งเสริมเกษตร</a></h3>
                                    <p>การบริหารจัดการข้อมูล ทำงานบนพื้นฐานของข้อมูลที่ถูกต้องสมบูรณ์เป็นปัจจุบันตรวจสอบได้และเป็นที่เชื่อถือยอมรับ...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/B4572.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">บ้านแสนรักษ์ เกษตรอินทรีย์ปลอดภัย</a></h3>
                                    <p>โครงการเกษตรอินทรีย์บ้านแสนรักษ์ มีหลักสูตรการทำงานตั่งแต่ปี 2549 เป็นหลักสูตรที่ให้ความรู้กับเกษตรกร...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            <div class="row">
                <div class="col-sm-6">
                    <h1>การศึกษา</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/045559Convenient19491224298_640c7f754f_k.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">"บิ๊กตู่" ปัญหาการศึกษาไทย - ขาดแคลนงบประมาณ</a></h3>
                                    <p>ทำเนียบรัฐบาล พล.อ.ประยุทธ์ จันทร์โอชา นายกรัฐมนตรีและหัวหน้าตณะรักษาความสงบแห่งชาติ "คสช."...</p>
                                </article>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius">
                            <div class="article__image border-tlr-radius">
                                <img src="img/ITgoodStudent-103.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <h3><a href="#">"บริษัทมหา"ลัย"... แนวโน้มการศึกษาไทย</a></h3>
                                    <p>ปัญหาการศึกษาบ้านเราซับซ้อนไม่ได้มีแค่มิติเดียวแต่มีหลายมิติ ล่าสุด"โอซีดี"จัดอันดับคุณภาพการศึกษาใน...</p>
                                </article>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <a href="content_list.php"><h4 style="color:gray;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            </div>

    </section>
    <!-- END row -->

    <section class="row" id="sec02">
        <div class="container">
            <div class="row">
                <h1><img src="img/c2_ico_vdo.png" alt="" />&nbspวิดีโอโฆษณา</h1>
                <div class="row">
                      <div class="col-xs-12 col-md-6">
                        <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/_xP3fI7yn5s?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                          <div style="cursor:pointer;" class="vid-item">
                            <div class="thumb">
                              <a href="" class=""><img src="http://img.youtube.com/vi/{{ $videos[0] -> video_idTH }}/0.jpg"></a>
                            </div>
                            <!-- <div class="desc">
                              Anakin Skywalker vs. Obi-Wan Kenobi
                            </div> -->
                            <div class="col-sm-6" style="float:none;margin: 0 auto;">
                                <p style="color:red;text-align:center;">{{ $videos[0] -> video_name_th}} <br>
                                  <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>

                            </div>
                          </div>
                        </div>

                      </div>

                      <!-- <div class="col-xs-12 col-md-6">
                        <a href="#" class="thumbnail">
                          <img src="img/iphone-6s-11.jpg" alt="...">
                        </a>
                        <div class="col-sm-6" style="float:none;margin: 0 auto;">
                            <p style="color:red;text-align:center;">โครงการปลูกต้นไม้เฉลิมพระเกียรติ...<br>
                              <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                            </p>

                        </div>

                      </div> -->
                      <div class="col-xs-12 col-md-6">
                        <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/eYT3ctPuVRw?autoplay=1;rel=0&amp;controls=0&amp;showinfo=0'">

                          <div style="cursor:pointer;" class="vid-item" >
                            <div class="thumb">
                              <a href="" class=""><img src="http://img.youtube.com/vi/{{ $videos[1] -> video_idTH}}/0.jpg"></a>
                            </div>
                            <!-- <div class="desc">
                              Anakin Skywalker vs. Obi-Wan Kenobi
                            </div> -->
                            <div class="col-sm-6" style="float:none;margin: 0 auto;">
                                <p style="color:red;text-align:center;">{{ $videos[1] -> video_name_th }}<br>
                                  <span style="color:#6F6F6F;font-size:0.7em;margin-top:0;">ดู 230,500 ครั้ง - 3 สัปดาห์ที่ผ่านมา...</span>
                                </p>

                            </div>
                          </div>
                        </div>

                      </div>
                  </div>
                
                <a href="content_list.php"><h4 style="color:#4c4c4c;text-align:right;padding:10px;top:0;">ดูเพิ่มเติม</h4></a>
            </div>
        </div>
    </section>
    <!-- END row -->

    <section class="row" id="envelope">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                        <div class="row product__content--480" style="padding:0 20%;text-align:center;">
                            <div class="col-sm-4">
                                <img src="img/c3_img_bottle_160.png" alt="" /><strong><p>ชนิดขวดแก้ว</p></strong><strong><span style="color:red;font-size:2em;line-height:0;">160ml
                        </span>  </strong>
                            </div>
                            <div class="col-sm-4">
                                <img src="img/c1_img_can01_can.png" alt="" style="width:55px;height:152px;" /><strong><p>ชนิดกระป๋อง</p></strong><strong><span style="color:red;font-size:2em;line-height:0;">330ml
                        </span>  </strong>
                            </div>
                            <div class="col-sm-4">
                                <img src="img/c3_img_bottle_220.png" alt="" /><strong><p>ชนิดขวด PET</p></strong><strong><span style="color:red;font-size:2em;line-height:0;">220ml
                        </span>  </strong>
                            </div>
                        </div>
                    </div>



                    
            <!-- <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <center><img src="img/siam_bottom.png" alt="" style="top:50%;margin-bottom: 20%; width: 70%;"/></center>
                    </div>
                </div>
            </div> -->
               <div class="container">
               <div class="row benefit_top--img">
                    <div class="col-md-12">
                        <center><img src="img/siam_bottom.png" alt="" style="margin-bottom: 20%; width: 70%;"/></center>
                    </div>
                </div>
                    <div class="row benefit_top benefit__ul--stroke" style="text-align:center;color:#fff;">
                        <div class="col-sm-6">
                            <p class="benefit_h1 stroke" style="font-size: 3em;"><strong style="margin-right: 70%;">ประโยชน์</strong></p>
                            <div class="row benefit_ul" style="text-align:left;">
                                <div class="col-sm-6">
                                    <ul class="color555" style="list-style:none;font-size:20px;">
                                        <li><a href="#111">ซูโครส</a>
                                        </li>
                                        <li><a href="#222">วิตามินบี 3</a>
                                        </li>
                                        <li><a href="#333">เด็กซ์แพนธินอล</a>
                                        </li>
                                        <li><a href="#444">วิตามินบี 12 </a>
                                        </li>
                                        <li><a href="#555">ทอรีน</a>
                                        </li>
                                        <li><a href="#666">คาแฟอีน</a>
                                        </li>
                                        <li><a href="#777">อินโนซิทอล</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 b6">
                                    <p id="111">ช่วยสังเคราะห์เม็ดเลือดแดง ช่วยในกระบวนการดูดซึม
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/vitaminB6.png" alt=""/>
                                    </p>
                                    <p id="222">วิตามินบี 3วิตามินบี 3วิตามินบี 3
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_02.png" alt=""/>
                                    </p>
                                    <p id="333">เด็กซ์แพนธินอลเด็กซ์แพนธินอลเด็กซ์แพนธินอล
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_03.png" alt=""/>
                                    </p>
                                    <p id="444">วิตามินบี 12วิตามินบี 12วิตามินบี 12
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_04.png" alt=""/>
                                    </p>
                                    <p id="555">ทอรีนทอรีนทอรีน
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_05.png" alt=""/>
                                    </p>
                                    <p id="666">คาแฟอีนคาแฟอีนคาแฟอีน
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_06.png" alt=""/>
                                    </p>
                                    <p id="777">อินโนซิทอลอินโนซิทอลอินโนซิทอล
                                        <img style="position:relative;z-index:1;width:276px;height:182px;" src="img/c3_img_benefit_image_07.png" alt=""/>
                                    </p>
                                </div>
                            </div>

                        <!-- <div class="container">

                            <div class="row b6_480">
                                <div class="col-xs-12" >
                                <div class="col-xs-6 none_padding col-sm-3 ">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_01.png" alt="" /><strong><h4 class="benefit--h4">ซูโครส</h4><h5 class="benefit--h5">เติมความสดชื่น ตื่นจากความอ่อนล้า</h5></strong>

                                </div>
                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_02.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B3</h4><h5 class="benefit--h5">ลดการเกิดความดันโลหิตสูง โรคไมเกรน </h5></strong>
                                </div>

                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_03.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B6</h4><h5 class="benefit--h5">ช่วยสังเคราะห์เม็ดเลือดแดง และการดูดซึม</h5></strong>
                                </div>
                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_04.png" alt="" /><strong><h4 class="benefit--h4">วิตามิน B12</h4><h5 class="benefit--h5">มีความสำคัญต่อเซลล์ ทุกชนิด </h5></strong>
                                </div>

                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_05.png" alt="" /> <br><strong><h4 class="benefit--h4">ทอรีน</h4><h5 class="benefit--h5">ดีต่อดวงตา </h5></strong>
                                </div>
                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_06.png" alt="" /><strong><h4 class="benefit--h4">คาเฟอีน</h4><h5 class="benefit--h5">ร่างกายสดชื่นตื่นตัวอยู่เสมอ </h5></strong>
                                </div>

                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_07.png" alt="" /> <br><strong><h4 class="benefit--h4">อินโนซิทอล</h4><h5 class="benefit--h5">สร้างเซลล์ในกระดูกอ่อน
                                    ลดการสะสมของไขมัน</strong></h5>
                                </div>
                                <div class="col-xs-6 none_padding col-sm-3">
                                  <img style="width:90px;height:90px;" src="img/c3_img_benefit_image_08.png" alt="" /><br><strong><h4 class="benefit--h4">เด็กซ์แพนธินอล</h4><h5 class="benefit--h5">บำรุงประสาทและสมอง </h5></strong>
                                </div>
                                </div>
                            </div>
                          </div> -->

                        </div>
                        <div class="col-sm-6">
                            <p class="benefit_h1 stroke" style="font-size: 3em;"><strong style="margin-left: 65%;">เหมาะกับคุณ</strong></p>
                            <div class="col-sm-6 b7">
                                <img style="width:350px ;margin-top:-30px; position:absolute;z-index:1;" src="img/dimi2-20140916160220.png" alt="" />
                                <img style="width:281px ;right: -50px;margin-top:10px; position:absolute;z-index:1;" src="img/dimi3-20140916160220.png" alt="" />
                                <div style="line-height: 1;font-size:1.3em;max-width:50%;color:#4c4c4c;text-align:left;">เครื่องดื่มอินทรีสยามเหมาะกับทุกเพศ ทุกวัย และทุกสาขาอาชีพ</div>
                                <div class="row" style="text-align:left;position:absolute;z-index:1;">
                                    <div class="row">
                                        <div class="col-md-3 ">
                                            <img src="img/asian-engineer-job-site-849x565.png" alt="" style="margin-left: -120px;position: absolute;width:200px;margin-top: 0px; " />
                                        </div>
                                        <div class="col-md-3 col-md-offset-1">
                                            <img style="margin-left: -140px;width:300px;margin-top: -30px;" src="img/DSC_0448-Copy.png" alt="" />
                                        </div>                         
                                    </div>
                                    
                                    
                                    <!-- <div class="who" style="display:absolute;"><a href="#">วัยรุ่น</a> <a href="#">คนสู้ชีวิต</a> <a href="#">วัยทำงาน</a>
                                    </div> -->

                                </div>
                                

                            </div>
                            <div class="row">
                                        <div class="col-md-12">
                                        <img style="bottom: -180px;z-index: 5;position: relative;width: 110%;right:-3%;" src="img/tabB7.png" alt="" />
                                        </div>
                                    </div>
                            <div class="container who--480 who--480--margin">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4">
                                        <div><img src="img/c3_img_target_a_1.png" alt="" style="width:201px;height:188px;" />
                                        </div>
                                        <img src="img/c3_img_benefit_indicator.png" alt="" />
                                        <div class="col-md-12 about_who">
                                            <p>สังโฆกาญจนาภิเษกแบดโคโยตีคำสาป ดีพาร์ทเมนท์ฮาโลวีนวีเจพาสเจอร์ไรส์เซอร์ไพรส์ วืดไวอะกร้าพาสตา รุสโซเอาท์ดอร์เวิร์ลด์ ออยล์แฟ็กซ์ แอร์สึนามิมิลค์ นู้ดเพนกวิน เธค คาเฟ่คูลเลอร์ โรลออนซะยิมแซ็กแคป ซิมมอลล์เอฟเฟ็กต์บอยคอตต์ </p>
                                        </div>

                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div><img src="img/c3_img_target_b_1.png" alt="" style="width:201px;height:188px;" />
                                        </div>
                                        <img src="img/c3_img_benefit_indicator.png" alt="" />
                                        <div class="col-md-12 about_who">
                                            <p>สังโฆกาญจนาภิเษกแบดโคโยตีคำสาป ดีพาร์ทเมนท์ฮาโลวีนวีเจพาสเจอร์ไรส์เซอร์ไพรส์ วืดไวอะกร้าพาสตา รุสโซเอาท์ดอร์เวิร์ลด์ ออยล์แฟ็กซ์ แอร์สึนามิมิลค์ นู้ดเพนกวิน เธค คาเฟ่คูลเลอร์ โรลออนซะยิมแซ็กแคป ซิมมอลล์เอฟเฟ็กต์บอยคอตต์ </p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div><img src="img/c3_img_target_c_1.png" alt="" style="width:201px;height:188px;" />
                                        </div>
                                        <img src="img/c3_img_benefit_indicator.png" alt="" />
                                        <div class="col-md-12 about_who">
                                            <p>สังโฆกาญจนาภิเษกแบดโคโยตีคำสาป ดีพาร์ทเมนท์ฮาโลวีนวีเจพาสเจอร์ไรส์เซอร์ไพรส์ วืดไวอะกร้าพาสตา รุสโซเอาท์ดอร์เวิร์ลด์ ออยล์แฟ็กซ์ แอร์สึนามิมิลค์ นู้ดเพนกวิน เธค คาเฟ่คูลเลอร์ โรลออนซะยิมแซ็กแคป ซิมมอลล์เอฟเฟ็กต์บอยคอตต์ </p>
                                        </div>
                                    </div>

                                </div>
                                
                            </div>
                        
                        </div>
                    
                    </div>
                </div>
                    <div class="row">
                            <div class="col-md-12">
                               <center> <p class="" style="font-size:3em;color:#555;">ติดต่อเรา</p> </center>
                            </div>
                        </div>
                        <div class="row" style="text-align:center;">
                            
                            <div class="col-sm-12">
                              <img style="width:280px;height:180px;" src="img/c001_img_logo.png" alt="" />
                            </div>
                            
                            <div class="col-sm-12">
                            <div>
                                <h1 style="color:gray;">02-510-3000</h1>
                            </div>
                            </div>
                            
                       </div>


                    

                </div>
            </div>
        </div>
@stop
