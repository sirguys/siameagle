@extends('layouts.main')
@section('title','inseefai')
@section('content')
<section id="hero-inseefai">
    <div class="container">
        <div class="row" style="">
            <div class="col-sm-12">
                <div id="" class=""><img class="img-responsive" src="img/text_Porduct.png" alt="" style="margin: 100px 0 0 185px!important;max-width:70%;" />
                </div>
                <div class="non_animated bottle"><img src="" alt="" />
                </div>
            </div>
            <!-- <div class="col-sm-6">
                <div id="animated-example" class="animated "><img src="" alt="" />
                </div>
                <div class="non_animated bottle"><img src="" alt="" />
                </div>
            </div> -->
        </div>
    </div>

    <span><img class="img-responsive can01_title--480" src="img/c1_img_can01_title_480.png" alt=""/> </span>
    <div class="container">
        
    </div>

    <div class="container">

        <div class="row" style="bottom:200px;">
            <div class="col-xs-12 div_480">
                <strong>  เติมความสดชื่น ตื่นจากความอ่อนล้า <br>เพิ่มการเผาผลาญไขมัน และฟื้นฟูในระดับเซลล์ <br>ใส่คำบรรยายสรรพคุณสินค้านี้ต่อไปเรื่อยๆ<br>
                 บำรุงสายตา ช่วยให้สดชื่น ตื่นตัวอยู่เสมอ <br>บำรุงระบบประสาทและสมอง และระบบขับถ่าย<br>ดื่มกี่ขวดก็ได้ไม่จำกัด</strong>
            </div>
        </div>
        <div class="row" style="top:200px;">
            <center><img class="img-responsive" src="img/251ml.png" alt="" style="margin-bottom:20px;"/></center>
        </div>

    </div>

</section>

<section class="inseefai">
    <div class="container" style="text-align:center;">
        <div class="row">
            <img src="img/inseefai_img.png" alt="" style="margin-bottom: 10%;" />
            
        </div>
        <div class="row" style="padding:0 20%;">
            <div class="col-sm-4">
              <img src="img/img_indeefai_can.png" alt="" style="width:70px;height:180px;" />
            </div>
            
            <div class="col-sm-4">
              <p style="background-color:#e91c28;color:#fff;margin:10px 15px;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px;">
                ส่วนประกอบที่สำคัญ
              </p>
            <div class="" style="margin:0 -40px;">
              วิตามินบี 3 0.012 % / วิตามินบี 5 0.004 % <br>
              วิตามินบี 6 0.004 % / วิตามินบี 12 0.0012 % <br>
              ไฟเบอร์ 1% / ทอรีน  0.532 % <br>
              อิโนซิตอล 0.532 % / นำตาล 8 % <br>
              เจือสีธรรมชาติและแต่งกลิ่นสังเคราะห์ ใช้วัตถุกันเสีย
            </div>
            </div>
            <div class="col-sm-4">
                <img src="img/img_indeefai_pel.png" alt="" style="width:70px;height:180px;" />
            </div>
        </div>
    </div>
</section>
@stop
