@extends('layouts.main')
@section('title','Contact')
@section('content')
<section id="hero-contact">
      <div class="container">

      <div class="aboutproduct" style="height:200px; text-align: right;margin-left: 50px; width:50%;transform:translate(95px,110px);">
            <div style="padding-top:1px;padding-right:100px; ">
            <h3>บริษัท 1688 เวิลด์ไวด์ จำกัด</h3>
            <h6>13/47 หมู่ 7 ถ.นวมินทร์ แขวงคลองทุ่ม เขตบึ่งกุ่ม กรุงเทพฯ 10230</h6>
            <h6>โทรศัพท์ : 02510-3000 โทรสาร : 02-944-7828</h6>
            <h6>E-mail : mok.co.th@gmail.com</h6>
            <h6>เวลาทำการ : 09:00 น. - 20:00 น. </h6>
            </div>
      
      </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6" style="margin-top:100px;">
                <h2 class="stroke" style="text-align: left; color:#000;">ติดต่อเรา</h2><br><br>
                
                  <h5 class="text-left stroke" style="color:#000;max-width: 60%;">กรุณากรอกรายละเอียดลงในช่องด้านล่างให้ครบถ้วนเพื่อความสะดวกรวดเร็ว ทางเราจะติดต่อกลับไปหาท่านอย่างเร็ววที่สุด</h5>
                
                
                <form>
                  <fieldset class="form-group" style="border:none;">
                    <input style="margin-bottom:5px;" type="text" class="form-control" id="formGroupExampleInput" placeholder="Topic">
                    <input style="margin-bottom:5px;" type="text" class="form-control" id="formGroupExampleInput" placeholder="name">
                    <input style="margin-bottom:5px;" type="text" class="form-control" id="formGroupExampleInput" placeholder="E-mail">
                    <input style="margin-bottom:5px;" type="text" class="form-control" id="formGroupExampleInput" placeholder="Tel.">
                     <textarea style="margin-bottom:5px;" class="form-control" id="exampleTextarea" rows="7" placeholder="Message"></textarea>
                      <button  type="submit" class="btn btn-primary pull-right" style="background-color:#555;color:#fff;border:none;">Send</button>
                  </fieldset>
                </form>
            </div>
            
            <div class="col-sm-6" style="margin-top:250px;color:#fff;">
            <h4 class="text-left stroke" style="color:#000;">แผนที่สำนักงานใหญ่</h4>
              <center>
<iframe class="thumbnail" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15498.204294898913!2d100.6486695!3d13.8059148!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa49e1ca16ad4e455!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5gOC4reC5h-C4oS7guYLguK0u4LmA4LiELiDguIjguLPguIHguLHguJQ!5e0!3m2!1sth!2sth!4v1449120810013" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </center>
            </div>
            </div>
            

        </div>
      </div>
</section>
            <!-- <div class="container">
                <div class="row" style="text-align:center;">
                    <div class="col-sm-6">
                      <img style="float:right;width:280px;height:180px;" src="img/c0_img_logo.png" alt="" />
                    </div>
                    <div class="col-sm-6">
                      <div style="float:left;text-align:left;">
                        <h1 style="color:#c91c28;">บริษัท 1168 เวิลด์ไวด์ จำกัด</h1>
                        <strong>ที่อยู่</strong> : 13/47 หมู่ 7 ถ.นวมินทร์ แขวงคลองกุ่ม เขตบึงกุ่ม กรุงเทพฯ 10230 <br>
                        <strong>โทรศัพท์</strong> : 02510-3000 <strong>โทรสาร</strong> : 02-944-7828 <br>
                        <strong>E-mail</strong> : mok.co.th@gmail.com <br>
                        <strong>เวลาทำการ</strong> : 09:00 น. - 20:00 น.

                      </div>
                    </div>
                </div>
            </div> -->

<!-- END row -->
@stop
