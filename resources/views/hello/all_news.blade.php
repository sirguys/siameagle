@extends('layouts.main')
@section('title','News')
@section('style')
@stop
@section('content')
<div class="sections">
    <section id="hero-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="image_news">
                    <img class="img-responsive" src="img/sci_18.jpg" alt="" style="height:490px;width:830px; margin:0 auto;" />
                        <h2>กิจกรรมรับน้องใหม่<br />สาขาวิชาชีววิทยา คณะวิทยาศาสตร์และเทคโนโลยี</h2> <br/>
                        <h4>การเรียนรู้ภาษาไทยทั้งในด้านพูดและเขียน สามารถนำไปสู้รายได้ที่สูงขึ้นและมีแนวโน้มสูงมากขึ้นในด้านการประกอบอาชีพของนักศึกษามหาวิทยาลัยกัมพูชา</h4>
                    </div>
                    <div class="Prapakorn-0">
        <h1>-JS Mookravee Italic</h1>
        <div class="font-container">
            <p class="letters">
                ฟกอทาดนกพสวายไพนปสสสสส<br />
                ABCDEFGHIJKLMNOPQRSTUVWXYZ <br />               0123456789.:,;()*!?'@#<>$%&^+-=~
            </p>
            <p class="s10" style="font-size: 10px;">การแข่งขันฟุตบอลชิงแชมป์สโมสรโลก ฟีฟ่า คลับ เวิลด์ คัพ ที่ประเทศญี่ปุ่นเมื่อวันที่ 16 ธ.ค. รอบรองชนะเลิศ....</p>
            <p class="s11" style="font-size: 11px;">Theการแข่งขันฟุตบอลชิงแชมป์สโมสรโลก ฟีฟ่า คลับ เวิลด์ คัพ ที่ประเทศญี่ปุ่นเมื่อวันที่ 16 ธ.ค. รอบรองชนะเลิศ... lazy dog.</p>
            <p class="s12" style="font-size: 12px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s14" style="font-size: 14px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s18" style="font-size: 18px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s24" style="font-size: 24px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s30" style="font-size: 30px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s36" style="font-size: 36px;">The quick brown fox jumps over the lazy dog.</p>
            <p class="s48" style="font-size: 48px;">เวิร์กทาวน์เฮาส์แป๋วบัลลาสต์วีไอพี สไตล</p>
            <p class="s60" style="font-size: 60px;">เวิร์กทาวน์เฮาส์แป๋วบัลลาสต์วีไอพี สไตล</p>
            <p class="s72" style="font-size: 72px;">เวิร์กทาวน์เฮาส์แป๋วบัลลาสต์วีไอพี สไตล์</p>
        </div>
    </div>
                    <!-- <div class="">
                    <img class="img-responsive" src="" alt="" style="width:80%; margin:0 auto;" />
                        
                    </div> -->
                    
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="padding-bottom:0;">
                        
        <div class="row">
<!--             <div class="col-md-6" style="color:#4c4c4c;">
 -->                

                <div class="row">
                <div class="col-sm-6">
                    <h1>คนทำงาน</h1>
                </div>
                <div class="col-sm-6">
                    <h1>กีฬา</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/shutterstock_96941162.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                    
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border" >Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/Surfing.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row"><hr></div>
            <!-- <a href="content_list.php"><h4 style="color:#4c4c4c;text-align:right;padding:10px;top:0;top:0;">ดูเพิ่มเติม</h4></a> -->
            <div class="row">
                <div class="col-sm-6">
                    <h1>เกษตรกร</h1>
                </div>
                <div class="col-sm-6">
                    <h1>การศึกษา</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/B4572.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="wrapper">
                        <div class="article radius ">
                            <div class="article__image border-tlr-radius">
                                <img src="img/10479682_4714297791833_721074049793301617_o.jpg" alt="image" class="border-tlr-radius">
                            </div>
                            <div class="article__content article__padding">
                                <article class="article">
                                    
                                    <a href="{{ url('news') }}">
                                      <p style="color: #000;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus harum...</p> 
                                    </a>
                                </article>
                            </div>
                            <ul class="list-group">
                              <li class="list-group-item text-gray all_new--non-border">Cras justo odio</li>
                              <li class="list-group-item text-gray all_new--non-border">Dapibus ac facilisis in</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
<!--             </div>
 -->        </div>
    </div>
    <hr/>
    

  </div>
  <!-- เปิด script jquery/1.6.2 เพื่อ effect เลื่อน "ข่าวสารแนะนำ" แต่จะมีปัญหา กับ scroll2top กระพริบ-->
  <!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
  <script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
  
@stop
