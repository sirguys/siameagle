<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div class="container">
      <div class="well bs-component">
        <form class="form-horizontal" method="POST" action="/auth/login">
          <fieldset>
            <legend><i class="fa fa-plus-square"></i> Member Login</legend>
            <div class="form-group {{ $errors->first('email') != '' ? 'has-error' : '' }}">
              <label for="inputEmail" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email" value="{{ old('email') }}">
              </div>
            </div>
            <div class="form-group {{ $errors->first('password') != '' ? 'has-error' : '' }}">
              <label for="inputPassword" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-5">
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-10 col-sm-offset-2">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Login</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </body>
</html>
