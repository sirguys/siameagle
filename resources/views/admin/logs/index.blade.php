@extends('layouts.admin.theme')

@section('page_title', 'Logs')
@section('content')
  <div class="page-header">
    <h3>
      Logs
    </h3>
  </div>

  <table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Date</th>
      <th>User</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php for ($i=0; $i <10 ; $i++) { ?>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
 <?php } ?>
    
  </tbody>
</table>



@stop

@section('custom-js')
  <script type="text/javascript">
    $(function() {
      $('a.delete').click(function() {
        if( confirm("Delete User") ) {
          return true;
        }
        else {
          return false;
        }
      })
    });
  </script>
@stop
