@extends('layouts.admin.theme')

@section('page_title', 'Convenient For')

@section('content')
  <div class="page-header">
    <h3>
      จัดการหน้า มาตรฐานการผลิต
      <a href="{{ url('admin/standard/create') }}"><small class="pull-right btn btn-default"><i class="fa fa-plus-square"></i> Create Standard</small></a>
    </h3>
  </div>
  <div class="row">
    <div class="col-md-12">
    </div>
  </div>
@stop
