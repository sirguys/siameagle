<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดข้อมูลองค์กร (TH)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_organizetion_th" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดข้อมูลองค์กร (EN)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_organizetion_en" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดข้อมูลองค์กร (CN)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_organizetion_cn" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดมาตรฐานการผลิต (TH)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_standard_th" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดมาตรฐานการผลิต (EN)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_standard_en" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รายละเอียดมาตรฐานการผลิต (CN)</label>
  <div class="col-sm-8">
    <textarea name="ps_detail_standard_cn" rows="6" class="form-control"></textarea>
  </div>
</div>
<div class="form-group">
  <label for="" class="col-sm-3 control-label">รูปภาพ</label>
  <div class="col-sm-6">
    <input type="file" name="ps_image" class="form-control" id="" placeholder="">
  </div>
</div>
