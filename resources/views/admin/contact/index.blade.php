@extends('layouts.admin.theme')

@section('page_title', 'Contact')
@section('content')
  <div class="page-header">
    <h3>
      จัดการหน้า ติดต่อเรา
      <a href="{{ url('admin/contact/create') }}"><small class="pull-right btn btn-default"><i class="fa fa-plus-square"></i> Create Contact</small></a>
    </h3>
  </div>
  <div class="row">
    <div class="col-md-12">
      @foreach($contacts as $index => $contact)
      <div class="panel panel-warning convenient">
        <div class="panel-heading">
          <h4>ข้อมูลติดต่อเรา</h4>
        </div>
        <div class="panel-body">
          <p>
            <strong>ชื่อบริษัท (TH) : </strong>
            <span>{{ $contact->company_name_th }}</span>
          </p>
          <p>
            <strong>ชื่อบริษัท (EN) : </strong>
            <span>{{ $contact->company_name_en }}</span>
          </p>
          <hr>
          <p>
            <strong>ที่อยู่บริษัท (TH) : </strong>
            <span>{{ $contact->company_address_th }}</span>
          </p>
          <p>
            <strong>ที่อยู่บริษัท (EN) : </strong>
            <span>{{ $contact->company_address_en }}</span>
          </p>
          <hr>
          <p>
            <strong>เบอร์โทรศัพท์ : </strong>
            <span>{{ $contact->company_tel }}</span>
          </p>
          <p>
            <strong>เบอร์โทรสาร : </strong>
            <span>{{ $contact->company_fax }}</span>
          </p>
          <hr>
          <p>
            <strong>E-mail : </strong>
            <span>{{ $contact->company_email }}</span>
          </p>
          <hr>
          <p>
            <strong>เวลาทำการ : </strong>
            <span>{{ $contact->company_fooice_house }}</span>
          </p>
          <hr>
          <p>
            <a href="{{ url("admin/contact/update/$contact->id") }}" class="btn btn-primary btn-md" role="button">อัพเดต</a>
          </p>
        </div>
      </div>
      @endforeach
    </div>
  </div>
@stop
