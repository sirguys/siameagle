<?php

return [
  't_report' => 'Report Problem',
  't_reviesd' => 'Revised Password',
  't_successful' => 'Successful Implementation',
  't_forgot' => 'Forgot Your Password',
  't_reset' => 'Password Reset',
  't_dashboards' => 'DashBoard',
  't_login' => 'Login',
  't_verified' => 'Not Verified',
  't_not' => 'Page Not Found',
  't_email' => 'E-mail Not Found',
  't_register' => 'Register',
  't_verified_succ' => 'Verified E-mail',
  't_profile' => 'Profole',
];
