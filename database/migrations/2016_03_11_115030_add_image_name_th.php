<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageNameTh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('convenients', function (Blueprint $table) {
            $table->string('convenient_image');
            $table->string('convenient_image_en');
            $table->string('convenient_image_cn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('convenients', function (Blueprint $table) {
            //
        });
    }
}
