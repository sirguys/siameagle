<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvenientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('convenient_name_th');
            $table->string('convenient_detail_th');
            $table->string('convenient_name_en');
            $table->string('convenient_detail_en');
            $table->string('convenient_name_cn');
            $table->string('convenient_detail_cn');
            

            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('convenients');
    }
}
