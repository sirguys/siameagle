<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name_th');
            $table->string('company_name_en');
            $table->string('company_address_th');
            $table->string('company_address_en');
            $table->string('company_tel');
            $table->string('company_fax');
            $table->string('company_email');
            $table->string('company_fooice_house');
            $table->string('company_google_map');
            $table->string('company_image_map');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}
