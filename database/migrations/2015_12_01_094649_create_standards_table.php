<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ps_detail_organizetion_th');
            $table->string('ps_detail_standard_th');
            $table->string('ps_detail_organizetion_en');
            $table->string('ps_detail_standard_en');
            $table->string('ps_detail_organizetion_cn');
            $table->string('ps_detail_standard_cn');
            $table->string('ps_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standards');
    }
}
